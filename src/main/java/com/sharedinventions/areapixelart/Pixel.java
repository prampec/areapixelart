package com.sharedinventions.areapixelart;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Pixel
{
    float positionXNorm;
    float positionYNorm;
    float widthNorm;
    float heightNorm;
    Color color;

    public Pixel(
        float positionXNorm,
        float positionYNorm,
        float widthNorm,
        float heightNorm)
    {
        this.positionXNorm = positionXNorm;
        this.positionYNorm = positionYNorm;
        this.widthNorm = widthNorm;
        this.heightNorm = heightNorm;
    }

    public static List<Pixel> buildPixels(int width, int height)
    {
        float cellWidthNorm = 1.0f / width;
        float cellHeightNorm = 1.0f / height;
        float xOffs = cellWidthNorm / 2;
        float yOffs = cellHeightNorm / 2;

        List<Pixel> result = new ArrayList<>(width * height);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float positionXPercent = cellWidthNorm * x + xOffs;
                float positionYPercent = cellHeightNorm * y + yOffs;

                result.add(new Pixel(
                    positionXPercent, positionYPercent,
                    cellWidthNorm, cellHeightNorm));
            }
        }

        return result;
    }

    public static List<Pixel> buildPixelsBrickStyle(int width, int height)
    {
        float cellWidthNorm = 1.0f / (width + 0.5f);
        float cellHeightNorm = 1.0f / height;
        float xOffs = cellWidthNorm / 2;
        float yOffs = cellHeightNorm / 2;

        List<Pixel> result = new ArrayList<>(width * height);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                float positionXNorm = cellWidthNorm * x + xOffs;
                if ((y%2) == 0)
                {
                    positionXNorm += xOffs;
                }
                float positionYNorm = cellHeightNorm * y + yOffs;

                result.add(new Pixel(
                    positionXNorm, positionYNorm,
                    cellWidthNorm, cellHeightNorm));
            }
        }

        return result;
    }
}
