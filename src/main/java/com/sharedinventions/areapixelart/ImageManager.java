package com.sharedinventions.areapixelart;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

import ij.process.ImageProcessor;
import ij.process.MedianCut;

public class ImageManager
{
    BufferedImage sourceImage;
    BufferedImage targetImage;
    private java.util.List<Pixel> pixels;
    int width;
    int height;
    private List<ColorProperties> colors = new ArrayList<>();

    public void openImage(File imageFile) throws IOException
    {
        sourceImage = ImageIO.read(imageFile);
    }

    public void rebuildTargetImage()
    {
        if (sourceImage != null)
        {
            rebuildPixels();
            fillPixelsFromImage();
            findDominantColors();
            drawPixelsToImage();
        }
    }

    private void rebuildPixels()
    {
//        pixels = Pixel.buildPixels(width, height);
        pixels = Pixel.buildPixelsBrickStyle(width, height);
    }

    private void fillPixelsFromImage()
    {
        float wVal = sourceImage.getWidth();
        float hVal = sourceImage.getHeight();

        for (Pixel pixel : pixels)
        {
            int x =
                Math.round(pixel.positionXNorm * wVal - (pixel.widthNorm * wVal / 2));
            int y =
                Math.round(pixel.positionYNorm * hVal - (pixel.heightNorm * hVal / 2));
            int w = Math.round(pixel.widthNorm * wVal);
            int[] rgbValues = sourceImage.getRGB(
                x, y, w,
                Math.round(pixel.heightNorm * hVal),
                null,
                0,
                w);
            //            int rgb = sourceImage.getRGB(
            //                (int) (pixel.positionXPercent * wVal),
            //                (int) (pixel.positionYPercent * hVal));
            pixel.color = averageColors(rgbValues);
        }
    }

    private Color averageColors(int[] rgbValues)
    {
        long reds = 0;
        long greens = 0;
        long blues = 0;
        for (int rgbValue : rgbValues)
        {
            Color c = new Color(rgbValue);
            reds += c.getRed();
            greens += c.getGreen();
            blues += c.getBlue();
        }
        Color sum = new Color(
            (int)(reds / rgbValues.length),
            (int)(greens / rgbValues.length),
            (int)(blues / rgbValues.length));

        return sum;
    }

    private void drawPixelsToImage()
    {
        float wVal = sourceImage.getWidth();
        float hVal = sourceImage.getHeight();
        targetImage = new BufferedImage(
            sourceImage.getWidth(),
            sourceImage.getHeight(),
            BufferedImage.TYPE_INT_ARGB);
        Graphics g = targetImage.getGraphics();

        for (Pixel pixel : pixels)
        {
            g.setColor(pixel.color);
            g.fillRect(
                Math.round((pixel.positionXNorm - pixel.widthNorm / 2) * wVal),
                Math.round((pixel.positionYNorm - pixel.heightNorm / 2) * hVal),
                Math.round(pixel.widthNorm * wVal)+1,
                Math.round(pixel.heightNorm * hVal)+1);
        }
    }

    public String getCellAspectRatio()
    {
        if (sourceImage == null)
        {
            return "-";
        }
        float ratio =
            (float)width / height *
                sourceImage.getWidth() / sourceImage.getHeight();
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return "1:" + df.format(ratio); // LEGO: 0.61
    }

    public int calculateCellHeight(int width, Double ratio)
    {
        double height = (double)width / ratio;
        if (sourceImage != null)
        {
            if (sourceImage.getHeight() > sourceImage.getWidth())
            {
                height *= (double) sourceImage.getWidth() / sourceImage.getHeight();
            }
            else
            {
                height /= (double) sourceImage.getWidth() / sourceImage.getHeight();
            }
        }
        return (int)Math.round(height);
    }

    public int calculateCellWidth(int height, Double ratio)
    {
        double width = (double)height * ratio;
        if (sourceImage != null)
        {
            if (sourceImage.getHeight() > sourceImage.getWidth())
            {
                width /= (double) sourceImage.getWidth() / sourceImage.getHeight();
            }
            else
            {
                width *= (double) sourceImage.getWidth() / sourceImage.getHeight();
            }
        }
        return (int)Math.round(width);
    }

    public void updateColors(List<ColorProperties> colorProperties)
    {
        this.colors = colorProperties;
    }

    private void findDominantColors()
    {
        int[] pixelArray =
            pixels.stream().mapToInt(p -> p.color.getRGB()).toArray();
        MedianCut medianCut = new MedianCut(pixelArray, width, height);
        ImageProcessor ip = medianCut.convertToByte(colors.size());
        int i = 0;
        for (Pixel pixel : pixels)
        {
            int color = ip.getColorModel().getRGB(ip.get(i++));
            pixel.color = new Color(color);
        }
    }

    int calculateColorDistance(Color color1, Color color2)
    {
        return
            Math.abs(color1.getRed() - color2.getRed()) +
            Math.abs(color1.getGreen() - color2.getGreen()) +
            Math.abs(color1.getBlue() - color2.getBlue());
    }
}
