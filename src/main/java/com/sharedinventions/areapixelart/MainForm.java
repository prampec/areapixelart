package com.sharedinventions.areapixelart;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;

public class MainForm
    extends JDialog
{
    private JButton loadButton;
    private JSpinner heightTextField;
    private JSpinner widthTextField;
    private JList<ColorProperties> colorList;
    private JButton addColorButton;
    private JButton modifyColorButton;
    private JButton removeColorButton;
    private JLabel pixelsCalc;
    private JPanel sourceImagePanel;
    private JPanel targetImagePanel;
    private JPanel contentPane;
    private JButton refreshButton;
    private JCheckBox keepCellRatioOfCheckBox;
    private JSpinner cellRatioSpinner;
    private JSpinner colorSpinner;
    private JFileChooser fileChooser = new JFileChooser();
    ImageManager imageManager = new ImageManager();

    public MainForm()
    {
        setContentPane(contentPane);
        setModal(true);
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(new FileFilter()
        {
            @Override
            public boolean accept(File f)
            {
                if (f.isDirectory()) {
                    return true;
                }

                String extension = Utils.getExtension(f);
                if (extension != null) {
                    if (extension.equals(Utils.tiff) ||
                        extension.equals(Utils.tif) ||
                        extension.equals(Utils.gif) ||
                        extension.equals(Utils.jpeg) ||
                        extension.equals(Utils.jpg) ||
                        extension.equals(Utils.png)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return false;
            }

            @Override
            public String getDescription()
            {
                return "Image files";
            }
        });

        loadButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                int dialogResult =
                    fileChooser.showOpenDialog(MainForm.this);
                if (dialogResult == JFileChooser.APPROVE_OPTION)
                {
                    openImage(fileChooser.getSelectedFile());
                }
            }
        });
        refreshButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                imageManager.rebuildTargetImage();
                targetImagePanel.repaint();
            }
        });
//        imageManager.height =
//            Integer.parseInt(heightTextField.getText());
//        imageManager.width =
//            Integer.parseInt(widthTextField.getText());
        widthTextField.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                imageManager.width = (Integer)widthTextField.getValue();
                updateHeightIfNeeded();
                calcPixels();
                imageManager.rebuildTargetImage();
                targetImagePanel.repaint();
            }
        });
        heightTextField.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                imageManager.height = (Integer) heightTextField.getValue();
                updateWidthIfNeeded();
                calcPixels();
                imageManager.rebuildTargetImage();
                targetImagePanel.repaint();
            }
        });
        imageManager.width = (Integer)widthTextField.getValue();
        imageManager.height = (Integer) heightTextField.getValue();

        final DefaultListModel<ColorProperties> listModel =
            (DefaultListModel<ColorProperties>) colorList.getModel();
        listModel.addListDataListener(new ListDataListener()
        {
            @Override
            public void intervalAdded(ListDataEvent e)
            {
                colorListUpdated();
            }

            @Override
            public void intervalRemoved(ListDataEvent e)
            {
                colorListUpdated();
            }

            @Override
            public void contentsChanged(ListDataEvent e)
            {
                colorListUpdated();
            }
        });

        listModel.addElement(new ColorProperties("color1"));
        listModel.addElement(new ColorProperties("color2"));
        listModel.addElement(new ColorProperties("color3"));
        listModel.addElement(new ColorProperties("color4"));
        listModel.addElement(new ColorProperties("color5"));
        addColorButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                listModel.addElement(new ColorProperties("colorX"));
            }
        });
        removeColorButton.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                int selectedIndex = colorList.getSelectedIndex();
                if (selectedIndex >= 0)
                {
                    listModel.removeElementAt(selectedIndex);
                }
            }
        });
        colorSpinner.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                int targetValue = (int) colorSpinner.getValue();
                while (targetValue < listModel.getSize())
                {
                    listModel.removeElementAt(listModel.getSize() - 1);
                }
                while (targetValue > listModel.getSize())
                {
                    listModel.addElement(new ColorProperties("ColorX"));
                }
            }
        });
    }

    private void updateHeightIfNeeded()
    {
        if (keepCellRatioOfCheckBox.isSelected())
        {
            Double ratio = (Double) cellRatioSpinner.getValue();
            heightTextField.setValue(
                    imageManager.calculateCellHeight(
                        imageManager.width, ratio));
        }
    }

    private void updateWidthIfNeeded()
    {
        if (keepCellRatioOfCheckBox.isSelected())
        {
            Double ratio = (Double) cellRatioSpinner.getValue();
            widthTextField.setValue(
                imageManager.calculateCellWidth(
                    imageManager.height, ratio));
        }
    }

    private void calcPixels()
    {
        String text =
            "Pixels: " + imageManager.width * imageManager.height +
            " Cell: " + imageManager.getCellAspectRatio();
        pixelsCalc.setText(text);
    }

    private void openImage(File imageFile)
    {
        try
        {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            imageManager.openImage(imageFile);

            targetImagePanel.repaint();
            updateHeightIfNeeded();
            calcPixels();

            imageManager.rebuildTargetImage();

            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            sourceImagePanel.repaint();
        }
        catch (IOException e)
        {
            JOptionPane.showMessageDialog(
                this, "Error loading image file '" + imageFile + "': " +
                    e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException
    {
        MainForm dialog = new MainForm();
        dialog.setIconImage(ImageIO.read(
            MainForm.class.getClassLoader().getResourceAsStream("icon.png")));
        dialog.pack();
        dialog.setVisible(true);

        System.exit(0);
    }

    private void createUIComponents()
    {
        sourceImagePanel = new JPanel()
        {
            @Override
            protected void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                imagePanelPainter(g, sourceImagePanel, imageManager.sourceImage);
            }
        };
        targetImagePanel = new JPanel()
        {
            @Override
            protected void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                imagePanelPainter(g, targetImagePanel, imageManager.targetImage);
            }
        };
        widthTextField = new JSpinner(new SpinnerNumberModel(30, 1, 300, 1));
        heightTextField = new JSpinner(new SpinnerNumberModel(30, 1, 300, 1));
        cellRatioSpinner = new JSpinner(
            new SpinnerNumberModel(0.61, 0.1, 80, 0.01));
        DefaultListModel<ColorProperties> listModel =
            new DefaultListModel<>();
        colorList = new JList<>(listModel);
        colorSpinner = new JSpinner(new SpinnerNumberModel(5, 2, 255, 1));
    }

    private void colorListUpdated()
    {
        ListModel<ColorProperties> listModel = colorList.getModel();
        java.util.List<ColorProperties> colorPropertiesList =
            new ArrayList<>(listModel.getSize());
        for (int i = 0; i < listModel.getSize(); i++)
        {
            colorPropertiesList.add(listModel.getElementAt(i));
        }
        imageManager.updateColors(colorPropertiesList);
        imageManager.rebuildTargetImage();
        targetImagePanel.repaint();
    }

    private void imagePanelPainter(
        Graphics g, JPanel imagePanel, BufferedImage image)
    {
        if (image == null)
        {
            return;
        }
        double scaleFactor = getScaleFactorToFit(
            new Dimension(
                image.getWidth(),
                image.getHeight()),
            imagePanel.getSize());
        g.drawImage(
            image, 0, 0,
            (int)(image.getWidth() * scaleFactor),
            (int)(image.getHeight() * scaleFactor),
            imagePanel);
    }

    private static class Utils
    {
        public final static String jpeg = "jpeg";
        public final static String jpg = "jpg";
        public final static String gif = "gif";
        public final static String tiff = "tiff";
        public final static String tif = "tif";
        public final static String png = "png";

        /*
         * Get the extension of a file.
         */
        public static String getExtension(File f)
        {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 &&  i < s.length() - 1) {
                ext = s.substring(i+1).toLowerCase();
            }
            return ext;
        }
    }

    public double getScaleFactor(int iMasterSize, int iTargetSize)
    {

        double dScale = 1;
        if (iMasterSize > iTargetSize)
        {
            dScale = (double) iTargetSize / (double) iMasterSize;
        }
        else
        {
            dScale = (double) iTargetSize / (double) iMasterSize;
        }

        return dScale;
    }

    public double getScaleFactorToFit(Dimension original, Dimension toFit)
    {
        double dScale = 1d;

        if (original != null && toFit != null) {

            double dScaleWidth = getScaleFactor(original.width, toFit.width);
            double dScaleHeight = getScaleFactor(original.height, toFit.height);

            dScale = Math.min(dScaleHeight, dScaleWidth);

        }

        return dScale;
    }
}
