package com.sharedinventions.areapixelart;

import java.awt.*;

public class ColorProperties
{
    int index;
    Color color;
    int numberOfCells = 0;
    String name;

    public ColorProperties(String name)
    {
        this.name = name;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public int getNumberOfCells()
    {
        return numberOfCells;
    }

    public void setNumberOfCells(int numberOfCells)
    {
        this.numberOfCells = numberOfCells;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name + ": " + numberOfCells;
    }
}
